// Janyl Jumadinova
// Quiz 1 Questions 8-9
// September 23, 2016

import java.util.Scanner;

public class Favourites
{
    public static void main(String args [])
    {
        String color = "";
        String food = "";
        String song = "";
        int birth = 0;
        int number = 0;

        Scanner input = new Scanner(System.in);

        System.out.println("Enter your favourite color: ");
        color = input.nextLine();

        System.out.println("Enter your favourite food: ");
        food = input.nextLine();

        System.out.println("Enter your favourite song: ");
        song = input.nextLine();

        System.out.println("Enter your favourite number: ");
        number = input.nextInt();

        System.out.println("Enter your birth year: ");
        birth = input.nextInt();

        System.out.println("You like "+color+"-colored "+food+" and "+number+" "+song+"s.");

    }
}

