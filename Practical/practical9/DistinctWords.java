//Zach Leonardo
//Practical 9
//4 November 2016
//
import java.util.*;
public class DistinctWords
{
    public static void main(String[] args)
    {
        ArrayList<String> words = new ArrayList<String>();
        Scanner in = new Scanner(System.in);
        while(in.hasNext())
        {
            String x = in.next().toLowerCase();
            if(!words.contains(x)){
                words.add(x);
            }
        }
        System.out.println("Number of distinct words: "+ words.size());
    }
}
