// Zach Leonardo
// Practical #2
// 09/09/16
//***************

import java.util.Date;

public class Practical2
{
	public static void main(String[] args)
	{
		System.out.println("Zach Leonardo\n Practical 2\n" + new Date() + "\n");
		System.out.println("  ______________");
		System.out.println("  \\________   __\\");
		System.out.println("           / /");
		System.out.println("          / /");
		System.out.println("         / /");
		System.out.println("        / /");
		System.out.println("       / /");
		System.out.println("  ____/ /______");
		System.out.println("  \\____________\\");
	}
}


