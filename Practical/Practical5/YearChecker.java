//Zach Leonardo
//7 October 2016
//Practical 5
//
//Designing a year checker.
import java.util.Scanner;
import java.util.Date;

public class YearChecker
{

    public static void main (String[]args)
    {
    System.out.println("Zach Leonardo\nPractical 5\n"+ new Date()+ "\n");
    //Declaration of variables
    int year;
    Scanner scan = new Scanner(System.in);

    //Prompting the user
    System.out.println("Enter a year between 1000 and 3000");
    year = scan.nextInt();

    //Testing for a leap year
    if ((year % 4 == 0) && (year % 100 != 0))
    {
       System.out.println(year+" is a leap year.");
    }
    else if ((year % 100 == 0) && (year % 400 == 0))
    {
        System.out.println(year+" is a leap year.");
    }
    else
        {
        System.out.println(year+ " is not a leap year.");
        }

    //Testing for 17 year cicadas broods
    if ((year-2013)%(17)==0)
    {
        System.out.println(year+ " is a year of the cicadas.");
    }
    else
    {
        System.out.println(year+ " is not a year of the cicadas");
    }

    }


}
