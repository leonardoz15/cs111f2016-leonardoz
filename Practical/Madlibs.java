
//****************************************
//Honor Code: This work is mine unless otherwise cited.
//Zachary Leonardo
//CMPSC 111 Fall 2016
//Practical #3
//Date: 16 Sept. 2016
//
//Purpose: To create a madlibs program
//****************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; //Imports a scanner
public class Madlibs
{
	//----------------------
	//main method: program execution begins here
	//----------------------
	public static void main(String[] args)
	{
	// Label output with name and date:
	System.out.println("Zach Leonardo\n Practical #3\n" + new Date() + "\n");
	//Declare variables
	String noun;
	String noun2;
	String noun3;
	String adjective;
	String adjective2;
	int num1;
	int num2;
	Scanner scan = new Scanner(System.in); //Declaring scanner object

	//Prompting the user
	System.out.println("Enter a singular noun: ");
	noun = scan.next();
	System.out.println(" Enter a plural noun: ");
	noun3 = scan.next();
	System.out.println("Enter another plural noun: ");
	noun2 = scan.next();
	System.out.println("Enter an adjective: ");
	adjective = scan.next();
	System.out.println("Enter another adjective: ");
	adjective2 = scan.next();
	System.out.println("Enter a non-zero whole number: ");
	num1 = scan.nextInt();
	System.out.println("Enter another non-zero whole number: ");
	num2 = scan.nextInt();
	
	//Putting together the madlib
	System.out.println("On the day of the party, you sent your " + adjective + " friend to go buy some chips for his " + noun + ". Instead, he purchased " + num1 + " bags of " + noun2 + "! He also bought " + num2 + " crates of " + adjective2 + " " + noun3 + "!");
	
	System.out.println("Now you have " + num1 + " bags of " + noun2 + ", and " + num2 + " crates of " + adjective2 + " " + noun3 + " for no reason!");

	
	}
}
	
