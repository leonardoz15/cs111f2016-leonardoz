//====================================
// CMPSC 111
// Practical 8
// 28 October 2016
//
// This program describes an octopus in the kitchen.
//====================================

import java.util.Date;

public class Practical8
{
    public static void main(String[] args)
    {
        System.out.println("Janyl Jumadinova\n" + new Date() + "\n");

        // Variable dictionary:
        Octopus ocky;           // an octopus
        Octopus james;
        Utensil spat;          // a kitchen utensil
        Utensil spat2;

        spat = new Utensil("spatula"); // create a spatula
        spat.setColor("green");        // set spatula properties--color...
        spat.setCost(10.59);           // ... and price

        ocky = new Octopus("Ocky",35);    // create and name the octopus
        //ocky.setAge(10);               // set the octopus's age...
        ocky.setWeight(100);           // ... weight,...
        ocky.setUtensil(spat);         // ... and favorite utensil.

        spat2 = new Utensil("metal spatula");
        spat2.setColor("Gold");
        spat2.setCost(59.99);

        james = new Octopus("James",27);
        james.setWeight(130);
        james.setUtensil(spat2);

        System.out.println("Testing 'get' methods:");
        System.out.println(ocky.getName() + " weighs " +ocky.getWeight()
            + " pounds\n" + "and is " + ocky.getAge()
            + " years old. His favorite utensil is a "
            + ocky.getUtensil());

        System.out.println(ocky.getName() + "'s " + ocky.getUtensil() + " costs $"
            + ocky.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat.getColor());

        // Use methods to change some values:

        ocky.setAge(20);
        ocky.setWeight(125);
        spat.setCost(15.99);
        spat.setColor("blue");

        System.out.println("\nTesting 'set' methods:");
        System.out.println(ocky.getName() + "'s new age: " + ocky.getAge());
        System.out.println(ocky.getName() + "'s new weight: " + ocky.getWeight());
        System.out.println("Utensil's new cost: $" + spat.getCost());
        System.out.println("Utensil's new color: " + spat.getColor());

        //James print out statements:
                System.out.println("\n"+james.getName() + " weighs " +james.getWeight()
            + " pounds\n" + "and is " + james.getAge()
            + " years old. His favorite utensil is a "
            + james.getUtensil());

        System.out.println(james.getName() + "'s " + james.getUtensil() + " costs $"
            + james.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat2.getColor());


    }
}
