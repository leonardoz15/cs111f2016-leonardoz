//*********************************************************************************
// CMPSC 111 Fall 2016
// Practical 10
//
// Purpose: Program demonstrating usage of the do while and for loops with the Iterator class
//*********************************************************************************
import java.util.*;

public final class LoopStyles {

    public static void main(String args[]) {
        ArrayList<String> aFlavours = new ArrayList<String>();
        aFlavours.add("chocolate");
        aFlavours.add("strawberry");
        aFlavours.add("vanilla");
        aFlavours.add("mint");
        aFlavours.add("cookies and cream");
        aFlavours.add("peanut butter");
        aFlavours.add("caramel");
        aFlavours.add("chocolate chip");
        aFlavours.add("pistachio");
        aFlavours.add("pecan");

        useDoWhileLoop(aFlavours);

        useForLoop(aFlavours);
    }

    private static void useDoWhileLoop(ArrayList<String> aFlavours) {
        Iterator<String> flavoursIter = aFlavours.iterator();
	// TO DO: rewrite this loop as a do..while loop
        do{
            System.out.println(flavoursIter.next());
        }
        while(flavoursIter.hasNext());
    }

    /**
     * Note that this for-loop does not use an integer index.
     */
    private static void useForLoop(ArrayList<String> aFlavours) {
        for (Iterator<String> flavoursIter = aFlavours.iterator(); flavoursIter.hasNext();){
            System.out.println(flavoursIter);
        }
    }
}

