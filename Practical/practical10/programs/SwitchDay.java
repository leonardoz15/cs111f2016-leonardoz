//*********************************************************************************
// CMPSC 111 Fall 2016
// Practical 10
//
// Purpose: Program demonstrating reading input from the terminal
//*********************************************************************************
import java.util.Scanner;

public class SwitchDay
{
    public static void main(String[] args)
    {
        Scanner s = new Scanner(System.in);
        System.out.print( "Enter a day of the week: "  );
        String day = s.next();
        day = day.toLowerCase();

    // TO DO: Using a switch statement, given the day of the week, print whether it is a weekday or a weekend day.
        switch(day)
        {
            case "saturday":
            case "sunday":
                System.out.println(day+" is a weekend day.");
                break;
            default:
                System.out.println(day+" is a weekday.");
        }

    }
}
