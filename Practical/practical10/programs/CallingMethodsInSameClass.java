//*********************************************************************************
// CMPSC 111 Fall 2016
// Practical 10
//
// Purpose: Program demonstrating  illustrates how to call static methods a class
// from a method in the same class. Static methods are the methods that can be called
// without creating an instance of the object.
//*********************************************************************************

public class CallingMethodsInSameClass
{
    public static void main(String args[])
    {
        printOne();
        printOne();
        printTwo();
        printThree();
    }

    public static void printOne()
    {
        System.out.println("Nothing is impossible, the word itself says \"I'm possible \" !");
    }

    public static void printTwo()
    {
        printOne();
        printOne();
    }

    public static void printThree()
    {
        printTwo();
    }

}
