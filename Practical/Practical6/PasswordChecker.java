
//*************************************
// CMPSC 111 Fall 2016
// Practical 6
// Name:Zachary Leonardo
// Purpose: Utilize if else statements to check the password validity
//*************************************

import java.util.Scanner;

public class PasswordChecker
{
	//-------------------------------
	// main method: program execution begins here
	//-------------------------------
	public static void main(String[] args)
	{

        //Declare and initialize variables
        Scanner scan = new Scanner(System.in);
        String input;
        String password;

        //Ask user if they want to pre-validate their password
        System.out.print("Would you like to pre-validate your new password? ");
        input = scan.nextLine();

				// use nested if/else structure
        if(input.equalsIgnoreCase("yes"))
        {
          System.out.print("Please enter a password: ");
          password = scan.nextLine();

					// check if the password contains any spaces
          if(password.contains(" "))
          {
            System.out.println("Fail: Your password contains a space!");
          }
          else
          {
            System.out.println("Pass: Your password does not contain a space!");
          }
                //Check for character length
          if(password.length() > 8)
          {
            System.out.println("Pass: Your password has at least 8 characters.");
          }
          else
          {
            System.out.println("Fail: Your password is less than 8 characters");
          }
                //Check for numbers
          if(password.contains("0")||password.contains("1")||password.contains("2")||password.contains("3")||password.contains("4")||password.contains("5")||password.contains("6")||password.contains("7")||password.contains("8")||password.contains("9"))
          {
            System.out.println("Pass: Your password contains at least one number.");
          }
          else
          {
              System.out.println("Fail: Your password needs at least one number.");
          }
                //Checking for - or _
          if(password.contains("-")||password.contains("_"))
          {
              System.out.println("Pass: Your password contains a - or _.");
          }
          else
          {
              System.out.println("Fail: Your password needs a - or _");
          }
                //Checking for uppercase
          if(!password.equals(password.toLowerCase()))
          {
              System.out.println("Pass: Your password contains at least one uppercase character.");
          }
          else
          {
              System.out.println("Fail: Your password needs at least one uppercase character.");
          }
                //Checking for lowercase
          if(!password.equals(password.toUpperCase()))
          {
              System.out.println("Pass: Your password contains at least on lowercase character.");
          }
          else
          {
              System.out.println("Fail: Your password needs at least one lower case character.");
          }
        }
				// if the user does not want to check the password, display
				// the requirements and save the user's password
        else
        {
          System.out.println("Password must meet these requirements:");
          System.out.println("   Must contain 8 characters.");
          System.out.println("   Must contain at least 1 numeric digit.");
          System.out.println("   Must contain at least 1 special character - or _.");
          System.out.println("   Must contain at least 1 upper case letter.");
          System.out.println("   Must contain at least 1 lower case letter. \n");
          System.out.print("Please enter a password: ");
          password = scan.next();

        }
	}
}
