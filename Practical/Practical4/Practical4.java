/************************************
  Honor Code: This work is mine unless otherwise cited.
  Zachary Leonardo
  CMPSC 111
  30 September 2016
  Practical 4

  Basic Input with a dialog box
 ************************************/

import javax.swing.JOptionPane;
import java.util.Random;

public class Practical4
{
    public static void main ( String[] args )
    {
        int age = 0; // user's input - age
        int age1 = 20; // modified age

        Random random = new Random();

        // display a dialog with a message
        JOptionPane.showMessageDialog( null, "Lets make your name Italian!" );

        // prompt user to enter the first name
        String name = JOptionPane.showInputDialog(" What is your last name?");

        //create a message with the modified first name
        String newName = "Your new last name is "+ name+"inni";

        //display the message with the modified user's name
        JOptionPane.showMessageDialog(null, newName);

        // 1: Prompt the user to get their first name and modify it to be Italian
        JOptionPane.showMessageDialog(null, "Lets make your first name Italian");
        String firstName = JOptionPane.showInputDialog("What is your first name?");
        String newFirstName = "Your new first name is "+ firstName +"o";
        JOptionPane.showMessageDialog(null, newFirstName);

        // 2: Prompt the user for their middle name and modify it
        JOptionPane.showMessageDialog(null, "Now lets change your middle name to be Italian too!");
        String middleName = JOptionPane.showInputDialog("What is your middle name?");
        String newMiddleName = "Your new middle name is "+middleName+"oni";
        JOptionPane.showMessageDialog(null,newMiddleName);

        //3: Enter your favorite food
        JOptionPane.showMessageDialog(null,"Now lets make your favorite food Italian too!");
        String food = JOptionPane.showInputDialog("Enter your favorite food");
        String newFood = "Your new favorite italian food is "+ food +" flavored pizza";
        JOptionPane.showMessageDialog(null, "Your new favorite food is "+newFood);


        // prompt user to enter the age
        age = Integer.parseInt(JOptionPane.showInputDialog("Enter your age"));

        // modify the age
        age1 += random.nextInt(40);
        String newAge = "Your age is: "+age1;

        // display a message with the new age
        JOptionPane.showMessageDialog(null, newAge);

        // TO DO: modify the above questions/answers to your own
        // TO DO: come up with your own (at least three) questions and answers

    } //end main
}  //end class Practical4
