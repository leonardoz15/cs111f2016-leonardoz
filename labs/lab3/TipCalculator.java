
//****************************************
//Honor Code: This work is mine unless otherwise cited.
//Zachary Leonardo
//CMPSC 111 Fall 2016
//Lab # 3
//Date: 15 Sept. 2016
//
//Purpose:To create a tip calculator using Scanner method
//****************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; //needed for using scanner

public class TipCalculator
{
	//----------------------
	//main method: program execution begins here
	//----------------------
	public static void main(String[] args)
	{
	//Declare objects
	String name;
	double bill = 0;
	int percentage = 0;
	double tip = 0;
	double totalbill = 0;
	int numPeople = 0;
	double share = 0;
	//Declare scanner object
	Scanner userInput = new Scanner(System.in);
	
	// Label output with name and date:
	System.out.println("Zachary Leonardo\n Lab #3\n" + new Date() + "\n");

	//Prompting the user to enter a name
	System.out.println("Please enter your name: ");
	name = userInput.next();
	
	System.out.println("Welcome to the tip calculator " + name + "!"); //Welcome message
	
	//Prompting the user to enter the bill
	System.out.println("Please enter the total bill amount: ");
	bill = userInput.nextDouble();

	//Prompting to enter tip
	System.out.println("Now enter the percentage (whole number) you want to tip: ");
	percentage = userInput.nextInt();

	System.out.println("Your bill before tip was: $" + bill); //display bill
	
	//Calculating the tip
	tip = ((double)percentage/100)*bill;
	System.out.println("Your tip amount is $" + tip); //Display tip
	
	//Calculate total bill
	totalbill = bill + tip;
	System.out.println("Your total bill is $" + totalbill); //Display total bill

	//People splitting the bill
	System.out.println("How many people will be splitting the bill?");
	numPeople = userInput.nextInt();
	
	//calculate bill per person
	share = totalbill/(double)numPeople;
	System.out.println("Each person should pay $" + share); //Display each share
	
	System.out.println("Thanks for using my tip calculator! Have a nice day!"); //Exit mesage
	}

}
