//=================================================
// Honor Code: This work is mine unless otherwise cited.

// Your Name [Replace with your name]
// CMPSC 111 Fall 2016
// Lab 4
// Date: mmm dd yyyy [fill in the date]
//
// Purpose: ... [describe the program]
//=================================================
public class Lab4
{	public static void main(String[] args)
	{	String s1 = "Computer Science";
		int x = 111;
		String s2 = s1 + " " + x;
		String s3 = " is fun";
		String s4 = s2 + s3;

		System.out.println("s1: " + s1);
		System.out.println("s2: " + s2);
		System.out.println("s3: " + s3);
		System.out.println("s4: " + s4);
	}
}
