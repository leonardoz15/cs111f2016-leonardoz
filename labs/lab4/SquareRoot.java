
//****************************************
//Honor Code: This work is mine unless otherwise cited.
//Zachary Leonardo
//CMPSC 111 Fall 2016
//Lab #4
//Date:22 Sept. 2016
//
//Purpose:TO calculate the square root
//****************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;
public class SquareRoot
{
	//----------------------
	//main method: program execution begins here
	//----------------------
	public static void main(String[] args)
	{
       //Declaring objects
    double value;
    double guess;
    Scanner input = new Scanner(System.in);
	   // Label output with name and date:
	System.out.println("Zachary Leonardo\nLab #4\n" + new Date() + "\n");
       //Prompting the user to enter a value
    System.out.println("Enter what you want to find the square root of: ");
    value = input.nextDouble();
    System.out.println("Enter a guess to what the square root is: ");
    guess = input.nextDouble();
      //Calculations
    guess = .5*(guess+(value/guess));
    guess = .5*(guess+(value/guess));
    guess = .5*(guess+(value/guess));
    guess = .5*(guess+(value/guess));

    System.out.println("The square root of " + value + " is " + guess + ".");
	}
}
