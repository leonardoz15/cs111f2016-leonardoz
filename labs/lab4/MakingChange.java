
//****************************************
//Honor Code: This work is mine unless otherwise cited.
//Zachary Leonardo
//CMPSC 111 Fall 2016
//Lab #4
//Date: 22 Sept. 2016
//
//Purpose:To create a program that calculates change given an integer in cents
//****************************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;
public class MakingChange
{
	//----------------------
	//main method: program execution begins here
	//----------------------
	public static void main(String[] args)
	{
	   // Label output with name and date:
    System.out.println("Zachary Leonardo\nLab #4\n" + new Date() + "\n");
       //Declaring objects
    int cents; //Value to make change for.
    int tens;
    int fives;
    int ones;
    int quarters;
    int dimes;
    int nickles;
    int pennies;
       //Declare scanner object
    Scanner input = new Scanner(System.in);
       //Input of cents
    System.out.println("Enter a whole number amount of cents to calculate: ");
    cents = input.nextInt();

    System.out.println("To make change for "+ cents + " cents, you need: ");
       //Calculation starts here
    tens = cents/1000; //number of cents in 10$ bills
    cents = cents % 1000; //cents remaining after tens removed
    fives = cents/500;
    cents = cents % 500;
    ones = cents/100;
    cents = cents % 100;
    quarters = cents/25;
    cents = cents % 25;
    dimes = cents/10;
    cents = cents % 10;
    nickles = cents/5;
    cents = cents % 5;
    pennies = cents/1;
       //Printing the output
    System.out.print(tens + " tens, " + fives + " fives, " + ones + " ones, " + quarters + " quarters, " + dimes + " dimes, " + nickles + " nickles, " + pennies + " pennies.\n");
	}
}
