//Zach Leonardo
//November 20, 2016
//Lab 8
//
import java.util.Scanner;

public class CarpetDesign {

    public static void main(String[] args)  {
       System.out.println("Enter a design character");
       Scanner input = new Scanner(System.in);
       char design = input.next().charAt(0);

        //Line 1
        for(int i=0; i<1; i++)
        {
            for(int j=0; j<30; j++)
            {
                System.out.print(design);
            }
            System.out.println();
        }

        //Checkerboard
        for(int l=0; l<1; l++)
        {
            for(int k=0; k<6; k++)
            {
                System.out.print(design);
            }
            System.out.print("      ");

            for(int k=0; k<6; k++)
            {
                System.out.print(design);
            }
            System.out.print("      ");

            for(int k=0; k<6; k++)
            {
                System.out.print(design);
            }
         }
        System.out.println();
        for(int p=0; p<1; p++)
        {
            for(int u=0; u<6; u++)
            {
                System.out.print(' ');
            }
            System.out.print(""+design+design+design+design+design+design);

            for(int u=0; u<6; u++)
            {
                System.out.print(' ');
            }
            System.out.print(""+design+design+design+design+design+design);

            for(int u=0; u<6; u++)
            {
                System.out.print(' ');
            }

        }
        System.out.println();
        for(int l=0; l<1; l++)
        {
            for(int k=0; k<6; k++)
            {
                System.out.print(design);
            }
            System.out.print("      ");

            for(int k=0; k<6; k++)
            {
                System.out.print(design);
            }
            System.out.print("      ");

            for(int k=0; k<6; k++)
            {
                System.out.print(design);
            }
         }
        System.out.println();


        //Straight line 2 and 3
        for(int t=0; t<2; t++)
        {
            for(int r=0; r<30; r++)
            {
                System.out.print(design);
            }
            System.out.println();
        }

        //Star
        for(int y=0; y<1; y++)
        {
            for(int u=0; u<14; u++)
            {
                System.out.print(' ');
            }
            System.out.print(design);

            for(int w=0; w<15; w++)
            {
                System.out.print(' ');
            }
            System.out.println();
        }
        for(int g=0; g<1; g++)
        {
            for(int h=0; h<13; h++)
            {
                System.out.print(' ');
            }
            System.out.print(""+design+design+design);

            for(int hy=0; hy<14; hy++)
            {
                System.out.print(' ');
            }
            System.out.println();
        }

        for(int hh=0; hh<1; hh++)
        {
            for(int v=0; v<10; v++)
            {
                System.out.print(' ');
            }
            System.out.print(""+design+design+design+design+design+design+design+design+design);

            for(int c=0; c<12; c++)
            {
                System.out.print(' ');
            }
            System.out.println();
        }

        for(int m=0; m<1; m++)
        {
            for(int b=0; b<12; b++)
            {
                System.out.print(' ');
            }
            System.out.print(""+design+design+design+design+design);

            for(int z=0; z<13; z++)
            {
                System.out.print(' ');
            }
            System.out.println();
        }

        for(int hh=0; hh<1; hh++)
        {
            for(int v=0; v<10; v++)
            {
                System.out.print(' ');
            }
            System.out.print(""+design+design+design+design+design+design+design+design+design);

            for(int c=0; c<12; c++)
            {
                System.out.print(' ');
            }
            System.out.println();
        }

        for(int g=0; g<1; g++)
        {
            for(int h=0; h<13; h++)
            {
                System.out.print(' ');
            }
            System.out.print(""+design+design+design);

            for(int hy=0; hy<14; hy++)
            {
                System.out.print(' ');
            }
            System.out.println();
        }

        for(int y=0; y<1; y++)
        {
            for(int u=0; u<14; u++)
            {
                System.out.print(' ');
            }
            System.out.print(design);

            for(int w=0; w<15; w++)
            {
                System.out.print(' ');
            }
            System.out.println();
        }

        //Straight lines 4 and 5
        for(int t=0; t<2; t++)
        {
            for(int r=0; r<30; r++)
            {
                System.out.print(design);
            }
            System.out.println();
        }

        //Checkerboard 2
        for(int l=0; l<1; l++)
        {
            for(int k=0; k<6; k++)
            {
                System.out.print(design);
            }
            System.out.print("      ");

            for(int k=0; k<6; k++)
            {
                System.out.print(design);
            }
            System.out.print("      ");

            for(int k=0; k<6; k++)
            {
                System.out.print(design);
            }
         }
        System.out.println();
        for(int p=0; p<1; p++)
        {
            for(int u=0; u<6; u++)
            {
                System.out.print(' ');
            }
            System.out.print(""+design+design+design+design+design+design);

            for(int u=0; u<6; u++)
            {
                System.out.print(' ');
            }
            System.out.print(""+design+design+design+design+design+design);

            for(int u=0; u<6; u++)
            {
                System.out.print(' ');
            }

        }
        System.out.println();
        for(int l=0; l<1; l++)
        {
            for(int k=0; k<6; k++)
            {
                System.out.print(design);
            }
            System.out.print("      ");

            for(int k=0; k<6; k++)
            {
                System.out.print(design);
            }
            System.out.print("      ");

            for(int k=0; k<6; k++)
            {
                System.out.print(design);
            }
         }
        System.out.println();
        //Straight line 7
        for(int p=0; p<1; p++)
        {
            for(int t=0; t<30; t++)
            {
                System.out.print(design);
            }
            System.out.println();
        }
    }
}
