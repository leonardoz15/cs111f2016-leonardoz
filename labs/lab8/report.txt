Zach Leonardo
November 10, 2016
Lab 8 Report

I used the given code as a model for producing the rest of the program. The
for loops were used to print out spaces and characters and each line began
another for loop. Once I got the hang of how the for loops worked the rest 
of the program was easy to copy and paste.'
***************************************************************************
Outputs:
leonardoz@aldenv186:~/cs111f2016/cs111f2016-leonardoz/labs/lab8$ javac *.javaleonardoz@aldenv186:~/cs111f2016/cs111f2016-leonardoz/labs/lab8$ java CarpetDesign
Enter a design character
O
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOO      OOOOOO      OOOOOO
      OOOOOO      OOOOOO      
OOOOOO      OOOOOO      OOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
              O               
             OOO              
          OOOOOOOOO            
            OOOOO             
          OOOOOOOOO            
             OOO              
              O               
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
OOOOOO      OOOOOO      OOOOOO
      OOOOOO      OOOOOO      
OOOOOO      OOOOOO      OOOOOO
OOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
leonardoz@aldenv186:~/cs111f2016/cs111f2016-leonardoz/labs/lab8$ javac *.javaleonardoz@aldenv186:~/cs111f2016/cs111f2016-leonardoz/labs/lab8$ java CarpetDesign
Enter a design character
*
******************************
******      ******      ******
      ******      ******      
******      ******      ******
******************************
******************************
              *               
             ***              
          *********            
            *****             
          *********            
             ***              
              *               
******************************
******************************
******      ******      ******
      ******      ******      
******      ******      ******
******************************

