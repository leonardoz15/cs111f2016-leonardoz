
//****************************************
//Honor Code: This work is mine unless otherwise cited.
//Zachary Leonardo
//CMPSC 111 Fall 2016
//Lab #2
//Date: 09/08/2016
//
//Purpose:Make a program that prints two words of wisdom
//****************************************
import java.util.Date; // needed for printing today's date

public class WordsofWisdom
{
	//----------------------
	//main method: program execution begins here
	//----------------------
	public static void main(String[] args)
	{
		// my first word of wisdom
		System.out.println("The squeaky wheel gets the grease");
		// My second word of wisdom
		System.out.println("Don't give up, The begining is always the hardest");

	   // Label output with name and date:
	   System.out.println("Zach Leonardo\n Lab #2\n" + new Date() + "\n");
	}
}
