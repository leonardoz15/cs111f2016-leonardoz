//=================================================
// Class Example using Graphics
// November 21, 2016
// Janyl Jumadinova
//
// Purpose: Illustrate Graphics class methods
//=================================================
import javax.swing.*;
import java.awt.*;
public class BasicGraphics extends javax.swing.JApplet
{
    public void paint(Graphics g)
    {
        g.setColor(Color.green);
        g.fillRect(0,0,700,700);
        g.setColor(Color.red);
        g.drawRect(70,10,400,300);
        g.setColor(Color.red);
        g.fillOval(150,75,150,200);


    }  // end paint()

	public static void main(String[] args)
    	{
        	JFrame window = new JFrame("Zach Leonardo ");

      		// Add the drawing canvas and do necessary things to
     		// make the window appear on the screen!
        	window.getContentPane().add(new BasicGraphics());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
		window.setSize(600, 400);

        	//window.pack();
    	}
} // end class BasicGraphics
