//This program takes the users three inputted numbers and puts them in
//order from largest to smallest.

import java.util.Scanner;

public class Order
{
    public static void main (String args[])
    {
        Scanner input = new Scanner(System.in);
        int x, y, z;

        System.out.println("Enter three numbers: ");
        x = input.nextInt();
        y = input.nextInt();
        z = input.nextInt();
        System.out.println("You entered: "+x+" , "+y+" , "+z);

        if(x<y && x<z)
        {
            System.out.println("Inside the first if statement");
            if(y<z)
            {
                System.out.println(x+", "+y+", "+z);
            }
            else {
                System.out.println(x+" , "+z+" , "+y);
            }
            System.out.println("End of the first if statement");
        }
        else if(y<x && y<z)
        {
            if (x<z)
            {
                System.out.println(y+", "+x+", "+z);
            }
            else{
                System.out.println(y+" , "+z+" , "+x);
            }
        }
        else if(z<x && z<y && x<y)
        {
            System.out.println(z+" , "+x+" , "+y);
        }
        else if(z<x && z<y && x>y)
        {
            System.out.println(z+" , "+y+" , "+x);
        }
    }
}
