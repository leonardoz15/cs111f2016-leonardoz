//Zach Leonardo
//19 Sept. 2016

import java.util.Scanner;

public class Addition
{
	public static void main(String args[])
	{
		String name = "";
		double first = 0;
		double second = 0;
		double sum = 0;
		
		Scanner input = new Scanner(System.in);

		System.out.println("Enter your full name: ");
		name = input.nextLine();
		System.out.println("You entered "+name);
		
		System.out.println("Enter the first number: ");
		first = input.nextDouble();
		
		System.out.println("Enter the second number: ");
		second = input.nextDouble();
		
		sum = first+second;
		System.out.println("Sum is "+sum);
	}
}
