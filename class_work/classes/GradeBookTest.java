// Create a class GradeBookTest to instantiate GradeBook class

public class GradeBookTest {
    public static void main (String args[]) {
        //System.out.println("start of the main");
        GradeBook myGradeBook  = new GradeBook ("CMPSC 111");
       // System.out.println("after creating an instance of GradeBook");
        //myGradeBook.displayMessage("CMPSC 111");
        System.out.println(myGradeBook.getCourseName());
        //System.out.println("after calling the method");
        myGradeBook.setCourseName("Music 101");
        System.out.println(myGradeBook.getCourseName());

    }
}
