// Define class GradeBook with a member method displayMessage

public class GradeBook {

    // variable declarations - instance variables
    private String courseName;

    //consructor
    public Gradebook(String initName)
    {
        courseName = initName;
    }
    //System.out.println("inside GradeBook");
    // method to display a welcome message
    // parameter - courseName
    public void displayMessage(String courseName) {
        System.out.println("Welcome to the Grade Book for "+courseName);
    }

    // get method
    public String getCourseName() {
        return courseName;
    }

    //set method
    public void setCourseName (String name)
    {
        courseName = name;
    }
}
