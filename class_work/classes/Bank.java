/* Bank class contains the main method.
It creates two instances of the Account object and uses
the method within the Account class
*/

import java.util.Scanner;
public class Bank
{
    public static void main ( String args[] )
    {
	// create instances of Account
        Account a1 = new Account ( 50.00 );
        Account a2 = new Account ( -7.53 );
        Account a3 = new Account ( 350.00 );
	// Call/invoke getBalance method in Account class
        System.out.println ("a1 balance: "+ a1.getBalance() );
        System.out.println ("a2 balance: "+ a2.getBalance() );
        System.out.println ("a3 balance: "+ a3.getBalance() );

        Scanner input = new Scanner ( System.in );
	// Get an input from the user
        System.out.print ( "Enter deposit for a1: ");
        double depositAmount = input.nextDouble();
        System.out.println ( "Adding "+ depositAmount+ " to a1" );
	// call/invoke credit method in Account class
        a1.credit ( depositAmount );

	// Call/invoke getBalance method in Account class
        System.out.println ("a1 balance: "+ a1.getBalance() );
        System.out.println ("a2 balance: "+ a2.getBalance() );
        System.out.println(" a3 balance: "+ a3.getBalance() );

	// Get an input from the user
        System.out.print ( "Enter deposit for a2: ");
        depositAmount = input.nextDouble();
        System.out.println ( "Adding "+ depositAmount+" to a2" );
	// Call/invoke credit method in Account class
        a2.credit ( depositAmount );
	// Call/invoke getBalance method in Account class
        System.out.println ("a1 balance: "+ a1.getBalance() );
        System.out.println ("a2 balance: "+ a2.getBalance() );
        System.out.println(" a3 balance: "+ a3.getBalance() );
     //Get input from user
        System.out.println("enter deposit for a3: ");
        depositAmount = input.nextDouble();
        System.out.println("Adding "+ depositAmount+" to a3");
        //call/invoke credit method
        a3.credit ( depositAmount );
        //Call/invoke getBalance
        System.out.println ("a1 balance: "+ a1.getBalance() );
        System.out.println ("a2 balance: "+ a2.getBalance() );
        System.out.println(" a3 balance: "+ a3.getBalance() );


    }
}

