//Zach Leonardo
//7 November 2016
//
public class Loops
{
    public static void main (String args[])
    {
        //while loop
        int count = 0;
        while(count < 100)
        {
            System.out.println(count);
            count += 10;
        }

        //do while loop
        count = 0;
        do
        {
            System.out.println(count);
            count += 10;
        }
        while(count < 100)

        //for loop
        for(count = 0; count < 100; count+=10)
        {
            System.out.println(count);
        }

        //nested loop
        for(int i=0; i<2; i++)
        {
            for(int j=0; j<3; j++)
            {
                for(int k=0; k<4; k++)
                {
                    System.out.println("i = "+i+" , j = "+j+" , k = "+k);
                }
            }
        }
    }
}
