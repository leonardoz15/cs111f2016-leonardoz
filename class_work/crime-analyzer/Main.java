//Zach Leonardo
//November 2, 2016
//
import java.util.Scanner;
import java.util.ArrayList;
import java.io.File; //to import the file
import java.io.IOException; //to handle errors

public class Main
{
    public static void main (String args[]) throws IOException
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("What would you like to search?");
        String keyword = scan.next();

        FileReader reader = new FileReader();

        reader.readFile(keyword);
        System.out.println(reader.getList());
        System.out.println(keyword+" occurred "+reader.getList().size()+" times");

    }
}
