// Zach Leonardo
// Class exercise. September 14th

import java.util.Scanner;

public class Age
{
	public static void main (String[] args)
	{
	double age;
	age = 20;
	
	double result = 0; //Declaration and assignment

	//Declare Scanner object
	Scanner userInput = new Scanner(System.in);
	
	//Prompting the user for input
	System.out.println("Enter an age: ");
	age = userInput.nextDouble();

	System.out.println("You entered :" +age);
	
	//Convert age to minutes
	result = age*365*24*60;
	System.out.println("Age in minutes: "+result);

	}
}
