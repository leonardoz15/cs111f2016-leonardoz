// Zachary Leonardo
/* "Hello World" program
* that prints "hello world"
*to the terminal
*/

public class Welcome
{
	public static void main (String args[])
	{
		System.out.println("Hello World!");
		System.out.println("Goodbye World");
		System.out.println("Adding "+ 12+23);
		System.out.println("Adding "+ (12+23));
	}
}
