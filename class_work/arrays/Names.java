//*******************
// CMPSC 111, Class Exercise
// November 14, 2016
//
// Janyl Jumadinova
// Purpose: Demonstrate the usage of loops and arrays to analyze names
//*********************
import java.io.*;
import java.util.Scanner;
import java.util.Arrays;

public class Names
{
	public static void main(String args[]) throws IOException
	{

		File file = new File("names.txt");
		Scanner input = new Scanner(file);

		// read from the file and save values into an array
		String [] names = new String[42];
		int count = 0;

        // process names.txt and save all Strings into an array
		while(input.hasNext())
		{
			String name = input.next();
			names[count] = name;
			count++;
		}

        // iterate through the array
        for(int i=0; i<names.length; i++)
        {
             if(names[i].startsWith("A"))
             {
                  System.out.println(names[i]);
             }
        }
        
        /*Arrays.sort(names);
        
        for(int i=0; i<names.length; i++)
        {
             System.out.println(names[i]);
        }*/
    }
}
