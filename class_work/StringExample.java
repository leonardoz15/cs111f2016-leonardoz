//Zachary Leonardo
//26 Sept. 2016
//
import java.util.Scanner;

public class StringExample
{
    public static void main (String args[])
    {
        //Declaration
        String word = "";
        int length = 0;
        char c;
        int index;
        Scanner input = new Scanner (System.in);

        //Get input from the user
        System.out.println("Enter a String: ");
        word = input.nextLine();

        System.out.println(word);

        //Get the length of the string
        length = word.length();
        System.out.println("length: "+length);

        c = word.charAt(4);
        System.out.println("Character at position 2: "+c);

        index = word.indexOf('a');
        System.out.println("index of character 'a': "+index);
    }
}
