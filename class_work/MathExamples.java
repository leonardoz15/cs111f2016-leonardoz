//Zach Leonardo
//Oct. 3 2016
//
public class MathExamples
{
    public static void main(String args[])
    {
    //use 5 math class methods
    int one = 15;
    int two = 68;
    int max;
    int min;
    double three = 2.75;
    double four;
    double five;
    double six;
    //cos of 2.75
    four = Math.cos(three);
    System.out.println("The cos of 2.75 is "+four);
    //max between integers
    max = Math.max(one,two);
    System.out.println("The max between 15 and 68 is "+max);
    //min between integers
    min = Math.min(one,two);
    System.out.println("The min between 15 and 68 is "+min);
    //Rounding up
    five = Math.round(three);
    System.out.println("2.75 rounded up is "+five);
    //Square root
    six = Math.sqrt(three);
    System.out.println("The square root of 2.75 is "+six);
    }
}
