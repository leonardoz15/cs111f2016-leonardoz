//October 26 2016
//Zach Leonardo
//While loop

import java.util.Scanner;

public class WhileMain
{
    public static void main (String args[])
    {
        Scanner scan = new Scanner (System.in);

        System.out.println("Enter a positive number: ");
        int count = 0;
        int number = 0;

        number = scan.nextInt();

        //Validating user input to ensure a positive number
        while(number < 0)
        {
            System.out.println("You need to enter a positive number");
            number = scan.nextInt();
            System.out.println("Iteration "+count+": user's input: "+number);
            count++;
        }

        //create instance of WhileExample
        WhileExample numIterate = new WhileExample(0);
        numIterate.setNum(number);
    }
}
