/**
 * A simple class to experiment with Swing graphics
 * @author bert
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GraphicsTester2 {
	public static void main(String [] args)
	{
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setLayout(new FlowLayout());

		JButton myButton = new JButton("I'm a JButton");
		final JTextField myTextField = new JTextField("I'm a JTextField");
		final JLabel myLabel = new JLabel("I'm a JLabel");

		myButton.addActionListener(new
				ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				myLabel.setText(myTextField.getText());
			}
		});

		frame.add(myButton);
		frame.add(myTextField);
		frame.add(myLabel);

		frame.pack();
		frame.setVisible(true);
	}
}
