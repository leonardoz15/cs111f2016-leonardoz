import java.awt.*;
import javax.swing.*;

public class LayoutMain
{
    public static void main(String [] args)
    {
        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout());

        frame.add(new JButton("Hello There"));
        frame.add(new JLabel("Hello Again"));
        frame.add(new JTextField("Hello Thrice"));

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(0,1));

        panel.add(new JButton("Ok go Away"));
        panel.add(new JLabel("Label 2"));
        panel.add(new JTextField("Text Field"));

        frame.add(panel);

        frame.pack();
        frame.setVisible(true);
    }
}
