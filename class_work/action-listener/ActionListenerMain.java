import javax.swing.*;
import java.awt.*;

public class ActionListenerMain
{
    public static void main(String args[])
    {
        //create frame
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Arrange from left to right
        frame.setLayout(new FlowLayout());

        //create a button
        JButton myButton = new JButton("That Was Easy");

        //create a textfield for user input
        JTextField myTextField = new JTextField("This is a text box");

        //create a label
        JLabel myLabel = new JLabel("Chicken Nugget");

        myButton.addActionListener(new SetTextListener(myTextField, myLabel));

        frame.add(myButton);
        frame.add(myTextField);
        frame.add(myLabel);

        frame.setVisible(true);

    }
}
