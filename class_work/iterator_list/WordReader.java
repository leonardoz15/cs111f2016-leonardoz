//Zach Leonardo
//October 31st 2016
import java.util.*;
import java.io.*;

public class WordReader
{
    public static void main (String args[]) throws IOException
    {
        ArrayList<String> words = new ArrayList<String>();
        File file = new File("words.txt");

        Scanner scan = new Scanner (file);

        while(scan.hasNext())
        {
            words.add(scan.next()); //add items form txt file to list
        }
        //display all words
        System.out.println(words);

        //print all words in the list backwards
        int count = words.size()-1;
        System.out.println("Count: "+count);
        while(count >= 0)
        {
            String w = words.get(count);
            System.out.println(w);
            count --;
        }

        //replace 's' with 'S'
        Iterator<String> wordsIt = words.iterator();
        while(wordsIt.hasNext())
        {
            String w = wordsIt.next();
            w = w.replace('s','S');
            System.out.println(w);
        }
    }
}
