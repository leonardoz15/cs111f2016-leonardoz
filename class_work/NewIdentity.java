//==========================================
// Janyl Jumadinova
// Class Exercise
// September 28, 2016
//
// Purpose: This program helps the user to create
// a new random identity
// ==========================================
import java.util.Scanner;
import java.util.Random;

public class NewIdentity
{

	public static void main(String args[])
   	{
        String firstName, lastName, job;
        int age;
        float salary = 100000;

        // create an instance of the Scanner
        Scanner scan = new Scanner (System.in);

        // create an instance of the Random class
        Random rand = new Random();

        // get the user's input
        System.out.print("Please enter a first name: ");
        firstName = scan.nextLine();
        System.out.print("Please enter a last name: ");
        lastName = scan.nextLine();
        System.out.print("Please enter your dream job: ");
        job = scan.nextLine();
        System.out.print("Please enter an integer: ");
        age = scan.nextInt();
        System.out.print("Please enter a number: ");
        salary = scan.nextFloat();

		// randomly change the user's input
		// get the length of the firstName string
		int length = firstName.length();
        int position = rand.nextInt(length);
        System.out.println("Position randomly generated is: "+position);
        char randomLetter = firstName.charAt(position);
        System.out.println("Random letter is: "+randomLetter);
		// get the position of the character in the firstName string

		// pick a character at that position

		// replace all of the selected characters with character 'a'
        firstName = firstName.replace(randomLetter, 'a');
        System.out.println("New first name is: "+firstName);
		// append "ov" to the lastName string
        lastName = lastName.concat("ini");

		// change the job to upper case
        job = job.toUpperCase();
		// assign a value for age
        age = rand.nextInt(age)+20;
		// assign a value for salary

		System.out.println("Your new name is "+firstName+" " +lastName+", and you are "+age+" years old. \nYou work as a "+job+ " making $"+salary+ " a year.");

   }
}
