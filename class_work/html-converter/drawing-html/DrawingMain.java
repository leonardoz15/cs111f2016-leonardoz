import javax.swing.*;
import java.awt.*;

public class DrawingMain extends JApplet
{
    public static void main(String [] args)
    {
        JFrame window = new JFrame("Janyl's drawing");
        window.getContentPane().add(new DrawingMain());
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setVisible(true);
        window.setSize(600,400);
    }

    public void paint (Graphics g)
    {
        // fill in the background
        g.setColor(Color.cyan);
        g.fillRect(0,0,800,400);

        DrawingCanvas drawing = new DrawingCanvas(g, 600, 400);
        drawing.drawSun();
        drawing.drawSnow();
        drawing.drawTree(430,200);
        drawing.drawTree(200,200);
        drawing.drawTree(50,200);
        drawing.drawTree(600,200);
    }
}

