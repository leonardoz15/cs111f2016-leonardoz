import java.io.IOException;
import java.util.Scanner;
import java.util.Date;
import java.io.File;
import java.util.ArrayList;

public class CalenderClockMain
{
    public static void main(String[] args) throws IOException
    {
        //Initialization:
    Scanner scan = new Scanner(System.in);
    CalenderList calenderList = new CalenderList();

    System.out.println("Welcome to the Calender Clock!\nPlease enter your first name: ");
    String name = scan.next();
    System.out.println("Hello "+name+" What action would you like to perform?");
    System.out.println("First type read hit enter and type the desired action");
    System.out.println("Available tasks: list, search_month, search_monthday, search_event, or quit");

    while(scan.hasNext())
        {
        String command = scan.nextLine();
            if(command.equals("read")) {
                calenderList.readCalenderItems();
            }
            else if(command.equals("list")) {
                calenderList.list();
            }
            else if(command.equals("search_month")) {
                System.out.println("What is the number of the month you are looking for?");
                int chosenMonth = scan.nextInt();
                calenderList.findEventByMonth(chosenMonth);
            }
            else if(command.equals("search_monthday")) {
                System.out.println("Enter the month");
                int chosenMonth = scan.nextInt();
                System.out.println("Enter the day");
                int chosenDay = scan.nextInt();
                calenderList.findEventByMD(chosenMonth,chosenDay);
            }
            else if(command.equals("search_event")) {
                System.out.println("What is a keyword of the event?");
                String chosenEvent = scan.nextLine();
                calenderList.findEvent(chosenEvent);
            }
            else if(command.equals("quit")) {
                break;
            }


        }
    }
}

